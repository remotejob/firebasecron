module gitlab.com/remotejob/firebasecron

go 1.13

require (
	cloud.google.com/go v0.50.0 // indirect
	cloud.google.com/go/firestore v1.1.0
	cloud.google.com/go/storage v1.4.0 // indirect
	firebase.google.com/go v3.11.0+incompatible
	github.com/golang/groupcache v0.0.0-20191027212112-611e8accdfc9 // indirect
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/hashicorp/golang-lru v0.5.3 // indirect
	github.com/jstemmer/go-junit-report v0.9.1 // indirect
	go.opencensus.io v0.22.2 // indirect
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553 // indirect
	golang.org/x/oauth2 v0.0.0-20191202225959-858c2ad4c8b6 // indirect
	golang.org/x/sys v0.0.0-20191220220014-0732a990476f // indirect
	golang.org/x/tools v0.0.0-20191220234730-f13409bbebaf // indirect
	google.golang.org/api v0.15.0
	google.golang.org/appengine v1.6.5 // indirect
	google.golang.org/genproto v0.0.0-20191220175831-5c49e3ecc1c1 // indirect
	google.golang.org/grpc v1.26.0 // indirect
)
