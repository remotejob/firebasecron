package main

import (
	"context"
	"log"
	"time"

	firebase "firebase.google.com/go"
	"gitlab.com/remotejob/firebasecron/pkg/getimages"
)

func main() {

	ctx := context.Background()

	conf := &firebase.Config{
		DatabaseURL: "https://fir-vuechatv6.firebaseio.com/",
	}
	app, err := firebase.NewApp(ctx, conf)
	if err != nil {
		log.Fatalf("firebase.NewApp: %v", err)
	}

	gclient, err := app.Database(ctx)
	if err != nil {
		log.Fatalf("app.Firestore: %v", err)
	}

	ref := gclient.NewRef("imgs")

	var rimges map[string]struct{}
	err = ref.Get(ctx, &rimges)
	if err != nil {
		log.Fatalf("app.Firestore Get: %v", err)

	}

	// log.Println(rimges)

	now := time.Now()

	images, err := getimages.Get("http://104.131.122.192:8001/api", "1")
	if err != nil {
		log.Fatalln(err)
	}
	if len(rimges) >= 4 {

		var keys []string
		for k, _ := range rimges {

			keys = append(keys, k)

		}
	

		m := make(map[string]interface{})

		// images[0].SendDate = now
		rimage := images[0]
		rimage.SendDate = now

		// imageToSend :=domains.ImageToSend{strconv.Itoa(rimage.Id),rimage.Name,strconv.Itoa(rimage.Age),rimage.City,rimage.Phone,rimage.Img_file_name,now}
		m[keys[0]] = rimage

		err = ref.Update(ctx, m)
		if err != nil {
			log.Fatalf("app.Firestore: %v", err)

		}

	} else {

		// images, err := getimages.Get("http://104.131.122.192:8001/api", "1")
		// if err != nil {
		// 	log.Fatalln(err)
		// }

		for _, img := range images {
			img.SendDate = now
			ref.Push(ctx, img)

		}


	}

	ref = gclient.NewRef("newimg").Child("last")
	images[0].SendDate = now
	ref.Set(ctx, images[0])

}
