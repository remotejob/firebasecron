package main

import (
	"context"
	"log"
	"time"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/db"
	"gitlab.com/remotejob/firebasecron/internal/domains"
)

var client *db.Client
var err error

// var ctx context.Context

func init() {
	// ctx = context.Background()
	// conf := &firebase.Config{
	// 	DatabaseURL: "https://fir-vuechat4.firebaseio.com/",
	// }
	// app, err := firebase.NewApp(ctx, conf)
	// if err != nil {
	// 	log.Fatalf("firebase.NewApp: %v", err)
	// }
	// client, err = app.Database(ctx)
	// if err != nil {
	// 	log.Fatalf("app.Firestore: %v", err)
	// }
}

func main() {

	ctx := context.Background()

	conf := &firebase.Config{
		DatabaseURL: "https://fir-vuechatv6.firebaseio.com/",
	}
	app, err := firebase.NewApp(ctx, conf)
	if err != nil {
		log.Fatalf("firebase.NewApp: %v", err)
	}

	client, err = app.Database(ctx)
	if err != nil {
		log.Fatalf("app.Firestore: %v", err)
	}

	ref := client.NewRef("room")

	var rooms map[string]struct{}
	err = ref.Get(ctx, &rooms)
	if err != nil {
		log.Fatalf("app.Firestore Get: %v", err)

	}

	for k, _ := range rooms {

		ref = client.NewRef("room").Child(k).Child("chats")

		var chats map[string]struct{}
		err = ref.Get(ctx, &chats)
		if err != nil {
			log.Fatalf("app.Firestore Get: %v", err)

		}

		now := time.Now().Unix()
		for k, _ := range chats {

			refn := ref.Child(k)
			// var msg map[string]struct{}
			var msg domains.Joinmsg
			err = refn.Get(ctx, &msg)
			if err != nil {
				log.Fatalf("app.Firestore Get: %v", err)

			}

			delta := now - msg.ServerTimeStamp

			//345600  96H
			if delta > 345600 {

				log.Println("delete", delta/3600)
				refn.Delete(ctx)

			}

		}

	}
	log.Println("rooms num",len(rooms))

}
